
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Person } from '../models/Person.Model' 
import { Contact } from '../models/Contact.Model'
@Injectable()
export class PersonService{
    constructor(private http: Http){}
    contact : Contact = {
        tel : ['1','2'],
        email : ['1','2']
    }
}