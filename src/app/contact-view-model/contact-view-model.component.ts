import { Component, OnInit ,Input} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Person } from '../../models/Person.Model';
import { Contact } from '../../models/Contact.Model';
import { Message } from '../../models/Message.Model';
import { ContactService } from '../contact.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-contact-view-model',
  templateUrl: './contact-view-model.component.html',
  styleUrls: ['./contact-view-model.component.css']
})
export class ContactViewModelComponent implements OnInit {
@Input() person : Person;
@Input() contact : Contact;
@Input() action : string;
isView = false;
message : Message = { message : [], error:null}; ;
  constructor(private contactService: ContactService){}

  ngOnInit() {
    console.log(this.action)
    if(this.action=="view")
      this.isView = true
  }

     onSave(){     
       console.log(this.contact)
       this.person.contact = this.contact
       console.log(this.person)
      //console.log(this.person)
      this.contactService.onInUpdate(this.person).subscribe(
        (response: Response) => {
          const data = response.json()
          if(data.error == false){
            window.location.href = '/';
          }
          else
            this.message = data;
        }
      );
      //searchService.onServiceSearch(this.keyTemp)
  }

  trackByIndex(index: number, obj: any): any {
    return index;
  }

  onAddInput(section:string){
    if(section == "P")
      this.contact.tel.push(null);  
    else if(section == "E")
      this.contact.email.push(null);
    else if(section == "T")
      this.person.team.push(null);
  }

  isHiddenAdd(section:string,index: number){
    if(section == "P")
      return this.contact.tel.length != index;
    else if(section == "E")
      return this.contact.email.length !=  index;
    else if(section == "T")
      return this.person.team.length != index;
  }

  onDeleteInput(section:string,index: number){
    if(section == "P")
      this.contact.tel.splice(index,1); 
    else if(section == "E")
      this.contact.email.splice(index,1); 
    else if(section == "T")
      this.person.team.splice(index,1); 
  }

  isDelete(section:string,index: number) {    
    if(section == "P")
      return this.contact.tel.length == 1;
    else if(section == "E")
      return this.contact.email.length == 1;
    else if(section == "T")
      return this.person.team.length == 1;
  }

}
