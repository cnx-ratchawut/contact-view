import { Component, OnInit,EventEmitter, Output } from '@angular/core';
import { ContactService } from '../contact.service'
import { Response } from '@angular/http';
@Component({
  selector: 'app-contact-search',
  templateUrl: './contact-search.component.html',
  styleUrls: ['./contact-search.component.css']
})
export class ContactSearchComponent implements OnInit {

  constructor(private searchService: ContactService){}
  @Output() oPersons = new EventEmitter<any[]>();
  keysearch : string
  keyTemp:string
  ngOnInit() {
  }
  onSearch(){
    this.keyTemp = (this.keysearch.trim()).length == 0 ? "==================================" : this.keysearch;
    this.searchService.onServiceSearch(this.keyTemp)
        .subscribe(
            (response: Response) => {
                const data = response.json()
                this.oPersons.emit(data);
            },
            (error) =>{
              this.oPersons.emit([]);
            }
        );
    
  }
}
