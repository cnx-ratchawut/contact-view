import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes,RouterModule } from "@angular/router"
import { AppComponent } from './app.component';
import { ContactListComponent } from './contact-list/contact-list.component';
import { ContactSearchComponent } from './contact-search/contact-search.component';
import { ContactAddComponent } from './contact-add/contact-add.component';
import { ContactHomeComponent } from './contact-home/contact-home.component';
import { ContactEditComponent } from './contact-edit/contact-edit.component';
import { ContactViewModelComponent } from './contact-view-model/contact-view-model.component';
import { ContactService } from './contact.service';
import { ContactViewComponent } from './contact-view/contact-view.component';
const appRoutes: Routes   = [
  { path: '', component : ContactHomeComponent },
  { path: 'add',component : ContactAddComponent},
  { path: 'edit/:id',component : ContactEditComponent},
  { path: 'view',component : ContactAddComponent},
  { path: 'view/:id',component : ContactViewComponent}
  
];
@NgModule({
  declarations: [
    AppComponent,
    ContactListComponent,
    ContactSearchComponent,
    ContactAddComponent,
    ContactHomeComponent,
    ContactEditComponent,
    ContactViewModelComponent,
    ContactViewComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ContactService],
  bootstrap: [AppComponent]
})
export class AppModule { }
