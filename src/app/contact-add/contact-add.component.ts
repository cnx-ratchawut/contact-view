import { Component, OnInit,Output } from '@angular/core';
import { Person } from '../../models/Person.Model' 
import { Contact } from '../../models/Contact.Model'
import { Response } from '@angular/http';
@Component({
  selector: 'app-contact-add',
  templateUrl: './contact-add.component.html',
  styleUrls: ['./contact-add.component.css']
})
export class ContactAddComponent implements OnInit {
 
  contact : Contact = {
      tel : [''],
      email : ['']
  } 
  @Output() oContact = new Contact();
  @Output() oPerson =  new Person();
  constructor(){}


  ngOnInit() {
    this.oContact = this.contact
  }
}


