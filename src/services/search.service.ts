import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class SearchService{
    constructor(private http: Http){}
  
    search(keyword:  string){
        const headser = new Headers({'Content-Type': 'application/json'})
        return this.http.get('http://localhost:5000/api/search?str='+keyword)
    }
}