import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  persons : any[]
  
  onAppSearch(persons:any[]){
    this.persons = persons
  }
}