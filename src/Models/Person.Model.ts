import { Contact } from './Contact.Model';
export class Person {
    public personId : string = "";
    public firstName: string = "";
    public lastName : string = "";
    public nickName : string = "";
    public contact : Contact;
    public team : any[] = [''];
    public office : string ="";
    public tags : any[]=[''];
}
