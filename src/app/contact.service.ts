import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
@Injectable()
export class ContactService{
    constructor(private http: Http){}
    onServiceDelete(p:object){
        return this.http.post('http://128.199.164.138/api/deletePerson',p)
    }
    onInUpdate(p:object){
        return this.http.post('http://128.199.164.138/api/inUpdatePerson',p)
    }
     onServiceGetPerson(p:object){
        return this.http.post('http://128.199.164.138/api/getPerson',p)
    }
    onServiceSearch(key:string){
        return this.http.get('http://128.199.164.138/api/search?str='+key)
    }
}