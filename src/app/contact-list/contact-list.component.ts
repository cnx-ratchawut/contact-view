import { Component, OnInit ,Input} from '@angular/core';
import { ContactService } from '../contact.service'
import { Response } from '@angular/http';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  @Input() listOfPersons : any[];
  constructor(private listService: ContactService){}

  ngOnInit() {
  }
  p : object;
  onDelete(id : string){
    this.p = {
        personId : id
    }

     this.listService.onServiceDelete(this.p).subscribe(
          (response: Response) => {
              const data = response.json()
              console.log(data);
          },
          (error) =>{
            console.log("error");
          }
        );     
  }

}
