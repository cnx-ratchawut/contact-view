import { Component, OnInit,EventEmitter, Output } from '@angular/core';
import { ActivatedRoute ,Router,Params} from '@angular/router';
import { Person } from '../../models/Person.Model';

import { ContactService } from '../contact.service';
import { Response } from '@angular/http';
import { Contact } from '../../models/Contact.Model';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css']
})
export class ContactEditComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private editService: ContactService
    ) { }
  @Output() oContact = new Contact();
  @Output() oPerson =  new Person();
  
  contact : Contact = {
      tel : [''],
      email : ['']
  }

  person:Person = {
        "personId" : "",
        "firstName": "",
        "lastName": "",
        "nickName": "",
        "contact": this.contact,
        "team": [''],
        "office": null,
        "tags": ['']
        };
  id: string
  ngOnInit() {
    this.person.personId = this.route.snapshot.params['id'];
    this.editService.onServiceGetPerson(this.person).subscribe(
        (response: Response) => {
          const data = response.json()
          if(data.personId==null){
            this.router.navigate(['/']);
            alert('ไม่พบข้อมูล')
          }
          this.oContact = data.contact;
          this.oPerson = data
          this.oPerson.contact = data.contact;
        }
      );
  }
  on
}
